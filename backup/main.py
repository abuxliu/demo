import cv2

if __name__ == '__main__':
    imagepath = r'./demo.jpg'
    # 获取训练好的人脸的参数数据，这里直接使用默认值
    face_cascade = cv2.CascadeClassifier(r'./haarcascade_frontalface_alt.xml')
    # 读取图片
    image = cv2.imread(imagepath)
    # 对图片灰度化处理
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # 检测人脸，探测图片中的人脸
    faces = face_cascade.detectMultiScale(
        gray,
        scaleFactor=1.15,
        minNeighbors=5,
        minSize=(5, 5)
    )
    # 标记人脸
    for (x, y, w, h) in faces:
        # 1.原始图片 2.人脸坐标原点 3.标记的高度 4，线的颜色 5，线宽
        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
        break

    # 显示图片
    cv2.imshow("Find Faces!", image)

    # 暂停窗口
    cv2.waitKey(5000)

    # 销毁窗口
    cv2.destroyAllWindows()
